import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { IVehiculo } from 'src/app/modelos/vehiculo.interface';
import { VehiculoService } from 'src/app/services/vehiculo.service';
import { AlertController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listar-vehiculo',
  templateUrl: './listar-vehiculo.page.html',
  styleUrls: ['./listar-vehiculo.page.scss'],
})
export class ListarVehiculoPage implements OnInit {
  listaVehiculos: IVehiculo[] = [];
  usuarioRol: string;

  constructor(private authService: AuthenticationService,
              private vehiculoService: VehiculoService,
              private alertController: AlertController,
              private toastController: ToastController,
              private router: Router) { }

  ngOnInit() {
    this.obtenerRolUsuarioActual();
    this.obtenerVehiculos();
  }

  private obtenerVehiculos() {
    this.vehiculoService.listarVehiculo()
      .subscribe(vehiculos => this.listaVehiculos = vehiculos);
  }

  logout() {
    this.authService.logout();
  }

  private obtenerRolUsuarioActual() {
    this.authService.obtenerUsuarioActual().then(usuario => {
      this.usuarioRol = JSON.parse(usuario).rol;
      console.log(this.usuarioRol);
    });
  }

  clickEditar(event: Event, indice: number) {
    event.stopImmediatePropagation();
    event.preventDefault();
    this.router.navigate(['/vehiculo', 'editar', indice]);
  }

  async clickEliminar(event: Event, indice: number) {
    event.stopImmediatePropagation();
    event.preventDefault();
    const alert = await this.alertController.create({
      header: '¡Advertencia!',
      message: '¿Está seguro que desea eliminar el registro?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel: Cancel');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            this.vehiculoService.eliminarVehiculo(indice)
              .subscribe(mensaje => {
                if (mensaje.estado) {
                  this.mostrarToast(mensaje.msg);
                }
              });
          }
        }
      ]
    });

    await alert.present();
  }

  private async mostrarToast(mensaje: string) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 3000,
      closeButtonText: 'Cerrar'
    });
    toast.present();
  }

}
