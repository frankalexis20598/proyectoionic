import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: './listar-vehiculo/listar-vehiculo.module#ListarVehiculoPageModule'
  },
  {
    path: 'crear',
    loadChildren: './crear-vehiculo/crear-vehiculo.module#CrearVehiculoPageModule'
  },
  {
    path: 'ver/:id',
    loadChildren: './detalle-vehiculo/detalle-vehiculo.module#DetalleVehiculoPageModule'
  },
  {
    path: 'editar/:id',
    loadChildren: './editar-vehiculo/editar-vehiculo.module#EditarVehiculoPageModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiculoModuleRoutingModule { }
