import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IVehiculo } from 'src/app/modelos/vehiculo.interface';
import { VehiculoService } from 'src/app/services/vehiculo.service';

@Component({
  selector: 'app-detalle-vehiculo',
  templateUrl: './detalle-vehiculo.page.html',
  styleUrls: ['./detalle-vehiculo.page.scss'],
})
export class DetalleVehiculoPage implements OnInit {
  vehiculo: IVehiculo = {};

  constructor(private activatedRouter: ActivatedRoute,
              private vehiculoService: VehiculoService) {}

  ngOnInit() {
    this.obtenerVehiculo();
  }

  obtenerVehiculo() {
    const indice = +this.activatedRouter.snapshot.paramMap.get('id');
    this.vehiculoService.obtenerVehiculoPorId(indice)
      .subscribe(data => this.vehiculo = data);
  }
}
