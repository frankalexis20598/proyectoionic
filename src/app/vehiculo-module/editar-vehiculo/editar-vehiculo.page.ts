import { Component, OnInit } from '@angular/core';
import { TipoVehiculoService } from 'src/app/services/tipo-vehiculo.service';
import { VehiculoService } from 'src/app/services/vehiculo.service';
import { AlertController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { ITipoVehiculo, IVehiculo } from 'src/app/modelos/vehiculo.interface';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-editar-vehiculo',
  templateUrl: './editar-vehiculo.page.html',
  styleUrls: ['./editar-vehiculo.page.scss'],
})
export class EditarVehiculoPage implements OnInit {
  listaTipoVehiculo: ITipoVehiculo[] = [];
  temporalVehiculo: IVehiculo = {};
  indiceVehiculo: number;
  temporalTipoVehiculo: number;

  constructor(private tipoVehiculoService: TipoVehiculoService,
              private vehiculoService: VehiculoService,
              private alertController: AlertController,
              private router: Router,
              private activatedRouter: ActivatedRoute) { }

  ngOnInit() {
    this.obtenerListaTipoVehiculo();
    this.obtenerVehiculo();
  }

  private obtenerListaTipoVehiculo() {
    this.tipoVehiculoService.listarTipoVehiculo()
      .subscribe(lista => this.listaTipoVehiculo = lista);
  }

  obtenerVehiculo() {
    this.indiceVehiculo = +this.activatedRouter.snapshot.paramMap.get('id');
    this.vehiculoService.obtenerVehiculoPorId(this.indiceVehiculo)
      .subscribe(data => {
        this.temporalVehiculo = this.clonar(data);
        this.temporalTipoVehiculo = this.temporalVehiculo.tipo.id;
      });
  }

  onSubmitVehiculo(vehiculoForm: NgForm) {
    if (vehiculoForm.valid) {
      this.temporalVehiculo.tipo = this.listaTipoVehiculo.find(tipo => {
        return tipo.id === this.temporalTipoVehiculo;
      });
      this.vehiculoService.actualizarVehiculo(this.indiceVehiculo, this.temporalVehiculo)
        .subscribe(mensaje => {
          if (mensaje.estado) { console.log(mensaje.msg); }
          this.router.navigate(['/vehiculo']);
        });
    } else {
      this.mostrarAlerta();
      console.log(vehiculoForm.valid, vehiculoForm.value);
    }
  }

  private async mostrarAlerta() {
    const alert = await this.alertController.create({
      header: '¡Advertencia!',
      message: 'Aún faltan algunos campos por rellenar.',
      buttons: ['OK']
    });

    await alert.present();
  }

  private clonar(objeto: any): any {
    const r: any = {};
    if (objeto == null) {
      return null;
    }
    for (const prop in objeto) {
      if (objeto[prop] != null && objeto[prop] instanceof Array) {
        const lista: any = objeto[prop];
        if (lista.length === 0) {
          r[prop] = [];
        }
        if (lista.length > 0) {
          if (typeof lista[0] === 'string' || typeof lista[0] === 'number') {
            r[prop] = lista;
          } else {
            r[prop] = this.clonar_lista(objeto[prop]);
          }
        }
      } else if (objeto[prop] instanceof Object) {
        r[prop] = this.clonar(objeto[prop]);
      } else {
        r[prop] = objeto[prop];
      }
      // r[prop] = this.clonar(objeto[prop]);
    }
    return r;
  }

  private clonar_lista(lista: any): any {
    const r: any = [];
    lista.forEach((element) => {
      const re = this.clonar(element);
      r.push(re);
    });
    return r;
  }

}
