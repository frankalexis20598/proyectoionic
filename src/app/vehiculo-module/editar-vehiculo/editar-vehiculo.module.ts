import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EditarVehiculoPage } from './editar-vehiculo.page';

const routes: Routes = [
  {
    path: '',
    component: EditarVehiculoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EditarVehiculoPage]
})
export class EditarVehiculoPageModule {}
