import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CrearVehiculoPage } from './crear-vehiculo.page';

const routes: Routes = [
  {
    path: '',
    component: CrearVehiculoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CrearVehiculoPage]
})
export class CrearVehiculoPageModule {}
