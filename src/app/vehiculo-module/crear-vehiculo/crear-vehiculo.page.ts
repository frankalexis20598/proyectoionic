import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ITipoVehiculo, IVehiculo } from 'src/app/modelos/vehiculo.interface';
import { TipoVehiculoService } from 'src/app/services/tipo-vehiculo.service';
import { VehiculoService } from 'src/app/services/vehiculo.service';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crear-vehiculo',
  templateUrl: './crear-vehiculo.page.html',
  styleUrls: ['./crear-vehiculo.page.scss'],
})
export class CrearVehiculoPage implements OnInit {
  listaTipoVehiculo: ITipoVehiculo[] = [];
  temporalVehiculo: IVehiculo = { propietario: {} };
  temporalTipoVehiculo: number;

  constructor(private tipoVehiculoService: TipoVehiculoService,
              private vehiculoService: VehiculoService,
              private alertController: AlertController,
              private router: Router) { }

  ngOnInit() {
    this.obtenerListaTipoVehiculo();
  }

  onSubmitVehiculo(vehiculoForm: NgForm) {
    if (vehiculoForm.valid) {
      this.temporalVehiculo.tipo = this.listaTipoVehiculo.find(tipo => {
        return tipo.id === this.temporalTipoVehiculo;
      });
      this.vehiculoService.guardarVehiculo(this.temporalVehiculo)
        .subscribe(mensaje => {
          if (mensaje.estado) { console.log(mensaje.msg); }
          this.router.navigate(['/vehiculo']);
        });
    } else {
      this.mostrarAlerta();
    }
  }

  private async mostrarAlerta() {
    const alert = await this.alertController.create({
      header: '¡Advertencia!',
      message: 'Aún faltan algunos campos por rellenar.',
      buttons: ['OK']
    });

    await alert.present();
  }

  private obtenerListaTipoVehiculo() {
    this.tipoVehiculoService.listarTipoVehiculo()
      .subscribe(lista => this.listaTipoVehiculo = lista);
  }
}
