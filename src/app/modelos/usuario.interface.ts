export interface IUsuario {
    id?: number;
    nombre?: string;
    rol?: string;
    correo?: string;
    password?: string;
}
