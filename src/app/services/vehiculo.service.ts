import { Injectable } from '@angular/core';
import { IVehiculo } from '../modelos/vehiculo.interface';
import { IMensaje } from '../modelos/mensaje.interface';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VehiculoService {

  private readonly LISTA_VEHICULOS: IVehiculo[] = [
    {
      tipo: { nombre: 'CARRO', id: 3 },
      marca: 'Toyota',
      color: 'Verde',
      placa: '458ESD',
      propietario: {
        nombre: 'Miguel',
        apellido: 'Vera',
        dni: '96548763',
        numLicencia: 'H54987563'
      }
    },
    {
      tipo: { nombre: 'CAMION', id: 2 },
      marca: 'Nissan',
      color: 'Rojo',
      placa: '458ESD',
      propietario: {
        nombre: 'Frank',
        apellido: 'Tomaylla',
        dni: '75689642',
        numLicencia: 'F65347912'
      }
    },
    {
      tipo: { nombre: 'MOTO', id: 1 },
      marca: 'Ford',
      color: 'Azul',
      placa: '458ESD',
      km: 100,
      tiempoUso: 1,
      motor: 'Motor monocilíndrico',
      propietario: {
        nombre: 'Steven',
        apellido: 'Huaman',
        dni: '86597458',
        numLicencia: 'E21356789'
      }
    },
    {
      tipo: { nombre: 'CARRO', id: 3 },
      marca: 'Hyundai',
      color: 'Plomo',
      placa: '458ESD',
      propietario: {
        nombre: 'Lucas',
        apellido: 'Torres',
        dni: '96587548',
        numLicencia: 'R56487965'
      }
    }
  ];
  mensaje: IMensaje = { estado: false };

  constructor() { }

  listarVehiculo(): Observable<IVehiculo[]> {
    return of(this.LISTA_VEHICULOS);
  }

  guardarVehiculo(vehiculo: IVehiculo): Observable<IMensaje> {
    this.LISTA_VEHICULOS.push(vehiculo);
    return of(this.construirMensaje('El registro se agrego correctamente.', true));
  }

  actualizarVehiculo(indice: number, vehiculo: IVehiculo): Observable<IMensaje> {
    this.LISTA_VEHICULOS[indice] = vehiculo;
    return of(this.construirMensaje('El registro se actualizó correctamente.', true));
  }

  eliminarVehiculo(indice: number): Observable<IMensaje> {
    this.LISTA_VEHICULOS.splice(indice, 1);
    return of(this.construirMensaje('El registro se elimino correctamente.', true));
  }

  construirMensaje(msg: string, estado: boolean): IMensaje {
    this.mensaje.msg = msg;
    this.mensaje.estado = estado;
    return this.mensaje;
  }

  obtenerVehiculoPorId(indice: number): Observable<IVehiculo> {
    return of(this.LISTA_VEHICULOS[indice]);
  }

}
