import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';
import { IUsuario } from '../modelos/usuario.interface';
import { IMensaje } from '../modelos/mensaje.interface';

const TOKEN_KEY = 'auth-token';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private readonly USUARIOS: IUsuario[] = [
    {
      id: 1,
      nombre: 'Frank Alexis',
      rol: 'ADMINISTRADOR',
      correo: 'admin@solmit.net',
      password: '12345678910'
    },
    {
      id: 2,
      nombre: 'Steven Andrés',
      rol: 'USUARIO',
      correo: 'user@solmit.net',
      password: '10987654321'
    }
  ];

  authenticationState = new BehaviorSubject(false);

  constructor(private storage: Storage, private platform: Platform) {
    this.platform.ready().then(() => {
      this.checkToken();
    });
  }

  checkToken() {
    this.storage.get(TOKEN_KEY).then(res => {
      if (res) {
        this.authenticationState.next(true);
      }
    });
  }

  login(correo: string, password: string): Observable<IMensaje> {
    const resultado = this.USUARIOS.find(usuario => {
      return (usuario.correo === correo && usuario.password === password);
    });
    if (resultado !== undefined) {
      this.storage.set(TOKEN_KEY, JSON.stringify(resultado)).then(() => {
        this.authenticationState.next(true);
      });
      return of(this.construirRespuesta('Inicio de Sesión exitoso!', true));
    } else {
      return of(this.construirRespuesta('Credenciales Incorrectas!', false));
    }
  }

  logout() {
    return this.storage.remove(TOKEN_KEY).then(() => {
      this.authenticationState.next(false);
    });
  }

  isAuthenticated() {
    return this.authenticationState.value;
  }

  private construirRespuesta(msg: string, estado: boolean) {
    const mensaje: IMensaje = {};
    mensaje.msg = msg;
    mensaje.estado = estado;
    return mensaje;
  }

  obtenerUsuarioActual() {
    return this.storage.get(TOKEN_KEY);
  }

}
