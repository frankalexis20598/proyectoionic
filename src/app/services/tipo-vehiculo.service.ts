import { Injectable } from '@angular/core';
import { ITipoVehiculo } from '../modelos/vehiculo.interface';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TipoVehiculoService {

  private readonly LISTA_TIPO_VEHICULO: ITipoVehiculo[] = [
    { id: 1, nombre: 'MOTO' },
    { id: 2, nombre: 'CAMION' },
    { id: 3, nombre: 'CARRO' }
  ];

  constructor() { }

  listarTipoVehiculo(): Observable<ITipoVehiculo[]> {
    return of(this.LISTA_TIPO_VEHICULO);
  }
}
