import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm = this.fb.group({
    correo: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(10)]]
  });

  get correo() { return this.loginForm.get('correo'); }
  get password() { return this.loginForm.get('password'); }

  constructor(private authService: AuthenticationService,
              private fb: FormBuilder,
              private toastController: ToastController) { }

  ngOnInit() {
  }

  onSubmitLogin() {
    this.authService.login(this.loginForm.value.correo, this.loginForm.value.password)
      .subscribe(mensaje => {
        if (mensaje.estado) {
          console.log(mensaje.msg);
          this.loginForm.reset();
        } else {
          this.mostrarToast(mensaje.msg);
        }
      });
  }

  async mostrarToast(mensaje: string) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 3000,
      closeButtonText: 'Cerrar'
    });
    toast.present();
  }

}
